#sve sam njeno kopirala 3.zadatak
from collections import Counter
from operator import countOf
from django.shortcuts import render, get_object_or_404
from django.contrib.auth.decorators import login_required
from django.http import HttpResponseRedirect
from django.urls import reverse
from django.db.models import Q
from app.models import Message, Account
from django.contrib.auth.models import User
from django.db.models import Count, Avg

from app.forms import MessageForm

# Create your views here.

# localhost:8000/messenger/
@login_required
def messenger(request):
    user = request.user

#3.zadatak
    """ unread_message_count = Message.objects.filter(receiver=user, is_read= False).count()
    """

    my_users = Message.get_thread_users(user)
    try:
        thread_user = my_users[0]
        return HttpResponseRedirect(reverse('messenger:thread', args=[thread_user.id]))
    except:
        thread_user = None

        context = {
            'sidebar_threads': [],
            'thread_user': None,
            'thread': None,
            'form': None,
        }
        return render(request, 'messenger/messenger.html', context)


# localhost:8000/messenger/1/ user_id
@login_required
def thread(request, user_id):
    thread_user = get_object_or_404(User, pk=user_id)
    user = request.user
    form = MessageForm()

#3.zadatak
    thread = Message.get_thread(user, thread_user)


    if request.method == 'POST':
        form = MessageForm(request.POST)
        if form.is_valid():
            message = Message(
                text = form.cleaned_data['text'],
                sender=request.user,
                receiver=thread_user,
            )
            message.save()

#3.zadatak
            if message.sender != thread_user:
                message.is_read = False
                message.save()


            return HttpResponseRedirect(reverse('messenger:thread', args=[thread_user.id]))



#3.zadatak
    if request.method == 'GET':
            unread_messages = [message for message in thread if message.sender != user and not message.is_read]
            if unread_messages:
                thread_user_message = unread_messages[-1]
                thread_user_message.is_read = True
                thread_user_message.save()
    #thread = Message.get_thread(user, thread_user)

    
    sidebar_threads = Message.get_threads_for(user)
    context = {
        'sidebar_threads': sidebar_threads,
        'thread_user': thread_user,
        'thread': thread,
        'form': form,
    }
    return render(request, 'messenger/messenger.html', context)

