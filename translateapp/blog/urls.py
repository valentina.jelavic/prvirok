#
from django.urls import path, include

from . import views

app_name = "blog"

urlpatterns = [
    path('', views.blog, name="blog"),
    path('new', views.new_blog, name="new_blog"),
    path('posts/', views.posts, name='posts'),
    path('post/<int:post_id>/', views.post_detail, name='post_detail'),
    path('user_posts/<int:user_id>/', views.user_posts, name='user_posts'),
    path('edit_post/<int:blog_id>/', views.edit_post, name='edit_post'),
    path('delete_post/<int:blog_id>/', views.delete_post, name='delete_post'),]