#za svaki html

from django.shortcuts import redirect, render
from django.core.exceptions import ObjectDoesNotExist

# Create your views here.
from django.shortcuts import render, get_object_or_404
from django.contrib.auth.decorators import login_required
from django.http import HttpResponseRedirect
from django.urls import reverse

from app.models import Blog, Account
from django.contrib.auth.models import User

from app.forms import BlogForm

# Create your views here.

@login_required
def blog(request) :
    latest_posts = Blog.objects.order_by('-publish_date')[:5]
    context = {
        'latest_posts': latest_posts
        }
    
    return render(request, 'blog/blog_home.html', context)


@login_required
def new_blog(request):
    if request.method == 'POST':
        form = BlogForm(request.POST, request.FILES)
        if form.is_valid():
            blog_post = form.save(commit=False)
            blog_post.user = request.user
            blog_post.save()
            return HttpResponseRedirect(reverse('blog:posts'))
        else:  
            return render(request, 'blog/new_blog.html', {'form': form})
    else:
        form = BlogForm()
        context = {
            'form': form,
        }
        return render(request, 'blog/new_blog.html', context)

@login_required
def posts(request):
    posts = Blog.objects.all()
    context = {
        'posts': posts
        }
    return render(request, 'blog/posts.html', context)


@login_required
def post_detail(request, post_id):
    post = get_object_or_404(Blog, pk=post_id)
    
    
    context = {
        'post': post,
    }
    return render(request, 'blog/post_detail.html', context)






@login_required
def user_posts(request,user_id):
    user_post= Blog.objects.filter(user_id=user_id)
    return render(request, 'blog/user_posts.html', {'user_post':user_post})

def edit_post(request,blog_id):
    blog = get_object_or_404(Blog, id=blog_id)
    if request.method == 'POST':
        blog_form = BlogForm(request.POST,request.FILES ,instance=blog)
        if blog_form.is_valid():
            blog_form.save()
            return HttpResponseRedirect(reverse('blog:post_detail', args=[blog_id]))
    else:
        blog_form = BlogForm(instance=blog)
    
    context = {
        'blog_form': blog_form
        }
    return render(request, 'blog/edit_post.html', context)


def delete_post(request, blog_id):
    blog = get_object_or_404(Blog, id=blog_id)

    if request.method == 'POST':
        blog.delete()
        return HttpResponseRedirect(reverse('blog:blog'))
      
    return redirect('blog:post_detail', post_id=blog_id)

