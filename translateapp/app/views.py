#opet sam sve kopirala 2.zadatak
from django.shortcuts import render, get_object_or_404
from django.http import HttpResponse, HttpResponseRedirect
from django.utils import timezone
from django.urls import reverse
from .models import Job, Bid, Dispute, Message, Rating, Account
from django.contrib.auth.models import User
from .forms import JobForm, BidForm, TranslationForm, DisputeForm, MessageForm
from django.contrib.auth.decorators import login_required
from django.db import transaction
from django.db.models import Q
from django.db.models import Count, Avg

# Create your views here.
def home(request):

    #2.zadatak
    trans_number=Account.objects.filter(translator=True).count()
    comp_job_number=Job.objects.filter(status=Job.Status.COMPLETED).count()
    average_rating=Rating.objects.aggregate(Avg('rating'))['rating__avg']
    average_rating = average_rating if average_rating is not None else 0.00
    context = {
        'trans_number':trans_number,
        'comp_job_number':comp_job_number,
        'average_rating':average_rating,
    }    


    return render(request, 'app/home.html', context)

@login_required
def jobs_index(request):
    if request.user.account.translator:
        # Get query parameters 
        # https://stackoverflow.com/questions/44598962/what-does-request-get-get-mean
        sort = request.GET.get('sort', "default")
        # The HTML form sends the language name, but the first option in the
        # form is "Any language", which is sent to the server as an empty
        # string (value="" in HTML). This line returns the GET
        # target_lang parameter if it is passed in the request, or None if it
        # is not passed.
        target_lang = request.GET.get('target_lang', None)

        # First we get all the jobs that are available
        jobs = Job.objects.filter(status='available')

        # Then we filter the jobs based on the target language
        if target_lang:
            jobs = jobs.filter(target_lang=target_lang)

        # Then we order the jobs based on the sort parameter
        if sort == "budget_asc":
            jobs = jobs.order_by('budget')
        elif sort == "budget_desc":
            jobs = jobs.order_by('-budget')
        else:
            jobs = jobs.order_by('id')

        jobs_w_bids = [ [job, job.bid_by_user(request.user)] for job in jobs ]

        # Available languages in Job.Language are stored as tuples of the form:
        # [ ('hr', 'Croatian'), ('en', 'English'), ... ]
        languages = Job.Language.choices
        context = {
            'jobs': jobs_w_bids,
            'languages': languages,
            'params': {
                'sort': sort,
                'target_lang': target_lang,
            },
        }
        return render(request, 'app/jobs_index.html', context)
    else:
        return HttpResponseRedirect(reverse('home_path'))

@login_required
def new_job(request):
    # if this is a POST request we need to process the form data
    if request.method == 'POST':
        # create a form instance and populate it with data from the request:
        form = JobForm(request.POST)
        # check whether it's valid:
        if form.is_valid():
            # save the form data to model and persist to db
            job = form.save(commit=False)
            # set the user to the current user
            job.user = request.user
            # set the status to available using the enum defined in the model class
            job.status = Job.Status.AVAILABLE
            # Persist the job to the db
            job.save()

            # redirect to job listing page:
            return HttpResponseRedirect(reverse('app:jobs'))
        else: 
            # if form is not valid, render the form with error messages
            # and the data the user entered. The error messages are contained
            # in the form.errors dictionary and can be accessed in the template.
            return render(request, 'app/new_job.html', {'form': form})
    else:
        # if a GET (or any other method) we'll create a blank form
        # and render it in the template like usual
        form = JobForm()
        context = {
            'form': form,
        }
        return render(request, 'app/jobs_new.html', context)

@login_required
def job_detail(request, job_id):
    job = get_object_or_404(Job, pk=job_id)
    form = BidForm(job)
    my_bid = job.bid_by_user(request.user)
    context = {
        'job': job,
        'form': form,
        'my_bid': my_bid,
    }
    return render(request, 'app/jobs_detail.html', context)

@login_required
def bid(request, job_id):
    if request.method == 'POST' and request.user.account.translator:
        job = get_object_or_404(Job, pk=job_id)
        form = BidForm(job, request.POST)
        if form.is_valid() and job.can_bid(request.user):
            bid = Bid(price=form.cleaned_data['price'], 
                    job=job, 
                    bidder=request.user)
            bid.save()
            return HttpResponseRedirect(reverse('app:job_detail', args=(job.id,)))
        else:
            return render(request, 'app/jobs_detail.html', {'job': job, 'form': form})

@login_required
def accept_bid(request, bid_id):
    bid = get_object_or_404(Bid, pk=bid_id)
    job = bid.job

    # check if the user is the job owner, otherwise anyone can accept any bid
    if request.method == 'POST' and job.user == request.user:
        bid.accepted = True
        bid.save()
        job.status = Job.Status.ASSIGNED
        job.save()
        return HttpResponseRedirect(reverse('accounts:dashboard'))

@login_required
def deliver_translation(request, bid_id):
    bid = get_object_or_404(Bid, pk=bid_id)
    job = bid.job

    if request.method == 'POST' and bid.accepted and request.user == bid.bidder:
        form = TranslationForm(request.POST)
        if form.is_valid():
            job.translation = form.cleaned_data['translation']
            job.status = Job.Status.COMPLETED
            job.save()
            bid.completed = True
            bid.save()
            return HttpResponseRedirect(reverse('accounts:dashboard'))
        else:
            return render(request, 'app/deliver_translation.html', {'job': job, 'form': form})
    else:
        form = TranslationForm()
        context = {
            'job': job,
            'form': form,
        }
        return render(request, 'app/deliver_translation.html', context)

@login_required
def bid_detail(request, bid_id):
    bid = get_object_or_404(Bid, pk=bid_id)
    job = bid.job
    form = DisputeForm()
    if request.user == bid.bidder or request.user == job.user:
        context = {
            'bid': bid,
            'job': job,
            'form': form,
        }
        return render(request, 'app/bid_detail.html', context)
    else:
        return HttpResponseRedirect(reverse('home_path'))

@login_required
def rate_bid(request, bid_id):
    bid = get_object_or_404(Bid, pk=bid_id)
    job = bid.job
    dispute_form = DisputeForm()

    if request.method == 'POST' and request.user == job.user:
        # Transaction is used to ensure that all the db operations are
        # atomic, ie. either all the operations succeed, or all of
        # them are rolled back. We want to avoid a situation where a
        # user rates a translation, gets tokens deducted, and then a
        # bug happens in saving the bidder balance and bidder never
        # gets the tokens. Tokens then remain in limbo, they were
        # deducted from the owner, but never arrived at the bidder's
        # account.
        #https://docs.djangoproject.com/en/4.1/topics/db/transactions/
        try: 
            with transaction.atomic():
                rating = Rating(
                    rating=request.POST['rating'],
                    rater=request.user,
                    rated=bid.bidder,
                    job=job,
                    bid=bid,
                )

                owner = request.user.account
                bidder = bid.bidder.account

                owner.balance -= bid.price
                owner.raise_if_invalid_balance()
                bidder.balance += bid.price
                bidder.raise_if_invalid_balance()

                owner.save()
                bidder.save()
                rating.save()

        except Exception as e:
            # Returning the user with the caught error in the context allows us
            # to display the error in the template. Check base.html for the
            # .alert classes. Also, we need to get the bid and job objects
            # again, because the transaction was rolled back and the objects
            # are no longer valid. The objects remained in memory and they retained
            # the reference the rating object, which never got saved to the db.
            bid = get_object_or_404(Bid, pk=bid_id)
            job = bid.job
            return render(request, 'app/bid_detail.html', {'bid': bid, 'job': job, 'from': form, 'error': e})


        return HttpResponseRedirect(reverse('app:bid_detail', args=[bid.id]))


@login_required
def raise_dispute(request, bid_id):
    bid = get_object_or_404(Bid, pk=bid_id)
    job = bid.job
    user = request.user

    if request.method == 'POST' and request.user == job.user:
        form = DisputeForm(request.POST)
        if form.is_valid():
            dispute = Dispute(
                reason=form.cleaned_data['reason'],
                job=job,
                bid=bid,
                user=user,
            )
            dispute.save()
            return HttpResponseRedirect(reverse('app:bid_detail', args=[bid.id]))
        else:
            bid = get_object_or_404(Bid, pk=bid_id)
            job = bid.job
            return render(request, 'app/bid_detail.html', {'bid': bid, 'job': job, 'form': form, 'error': e})



@login_required
def send_message(request, receiver_id):
    receiver = get_object_or_404(User, pk=receiver_id)
    sender = request.user

    if request.method == 'POST': 
        form = MessageForm(request.POST)
        if form.is_valid():
            message = Message(
                text=form.cleaned_data['text'],
                sender=sender,
                receiver=receiver,
                )
            message.save()
            return HttpResponseRedirect(reverse('accounts:dashboard'))
        else:
            form = MessageForm()
            return render(request, 'app/send_message.html', {'receiver': receiver, 'form': form})

    form = MessageForm()
    context = {
            'receiver': receiver,
            'form': form,
    }
    return render(request, 'app/send_message.html', context)

